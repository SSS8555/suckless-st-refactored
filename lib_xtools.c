#include <unistd.h>
#include <errno.h>
#include <stdarg.h>

#include "int_types.h"
#include <stdint.h>
#include <sys/types.h>

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
/**h************************************************************************/
void die(const char *errstr, ...)
{
	va_list ap;

	va_start(ap, errstr);
	vfprintf(stderr, errstr, ap);
	va_end(ap);
	exit(1);
}
/**h************************************************************************/
ssize_t xwrite(int fd, const char *s, size_t len)
{
	size_t aux = len;
	ssize_t r;

	while (len > 0) {
		r = write(fd, s, len);
		if (r < 0)
			return r;
		len -= r;
		s += r;
	}

	return aux;
}
/**h************************************************************************/
void * xmalloc(size_t len)
{
	void *p;

	if (!(p = malloc(len)))
		die("malloc: %s\n", strerror(errno));

	return p;
}
/**h************************************************************************/
void * xrealloc(void *p, size_t len)
{
	if ((p = realloc(p, len)) == NULL)
		die("realloc: %s\n", strerror(errno));

	return p;
}
/**h************************************************************************/
char * xstrdup(const char *s)
{
	char *p;

	if ((p = strdup(s)) == NULL)
		die("strdup: %s\n", strerror(errno));

	return p;
}
/***************************************************************************/
