#define UTF_INVALID   0xFFFD
#define UTF_SIZ       4
size_t utf8decode(const char *c, Rune *u, size_t clen);
size_t utf8encode(Rune u, char *c);
