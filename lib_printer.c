#include <stdio.h>
#include <unistd.h>  // xwrite() only

#include "int_types.h"
#include <stdint.h>
#include <sys/types.h>

#include "lib_xtools.h"
#include "lib_stdata.h"
#include "st_macros.h"
#include "lib_utf8.h"

/**e************************************************************************/
int iofd = 1;
/***************************************************************************/

/**h************************************************************************/
void tprinter(char *s, size_t len)
{
	if (iofd != -1 && xwrite(iofd, s, len) < 0) {
		perror("Error writing to output file");
		close(iofd);
		iofd = -1;
	}
}
/**h************************************************************************/
void tdumpline(int n)
{
	char buf[UTF_SIZ];
	const Glyph_ *bp, *end;

	bp = &term.line[n][0];
	end = &bp[MIN(tlinelen(n), term.col) - 1];
	if (bp != end || bp->u != ' ') {
		for ( ; bp <= end; ++bp)
			tprinter(buf, utf8encode(bp->u, buf));
	}
	tprinter("\n", 1);
}

/**h************************************************************************/
void tdump(void)
{
	int i;

	for (i = 0; i < term.row; ++i)
		tdumpline(i);
}

/***************************************************************************/
