void selinit(void);
void selstart(int col, int row, int snap);
void selextend(int col, int row, int type, int done);
int selected(int x, int y);
char * getsel(void);
void selclear(void);
void selscroll(int orig, int n);
void tdumpsel(void);
