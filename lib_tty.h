int ttynew(const char *line, char *cmd, const char *out, char **args);
size_t ttyread(void);
void ttywriteraw(const char *s, size_t n);
void ttyhangup();
