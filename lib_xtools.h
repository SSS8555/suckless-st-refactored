void die(const char *errstr, ...);
ssize_t xwrite(int fd, const char *s, size_t len);
void * xmalloc(size_t len);
void * xrealloc(void *p, size_t len);
char * xstrdup(const char *s);
