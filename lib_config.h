#include <limits.h>
#include <X11/Xlib.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>

#include "int_types.h"
/***************************************************************************/
enum selection_mode {
	SEL_IDLE = 0,
	SEL_EMPTY = 1,
	SEL_READY = 2
};

enum selection_type {
	SEL_REGULAR = 1,
	SEL_RECTANGULAR = 2,
	SEL_MAX = 3
};

enum selection_snap {
	SNAP_WORD = 1,
	SNAP_LINE = 2
};
/***************************************************************************/
/* types used in config.h */
typedef union {
	int i;
	uint ui;
	float f;
	const void *v;
	const char *s;
} Arg;

typedef struct {
	uint mod;
	KeySym keysym;
	void (*func)(const Arg *);
	const Arg arg;
} Shortcut;

typedef struct {
	uint mod;
	uint button;
	void (*func)(const Arg *);
	const Arg arg;
	uint  release;
} MouseShortcut;

typedef struct {
	KeySym k;
	uint mask;
	char *s;
	/* three-valued logic variables: 0 indifferent, 1 on, -1 off */
	signed char appkey;    /* application keypad */
	signed char appcursor; /* application cursor */
} Key;
/***************************************************************************/
#include "lib_shortcuts.h"
#include "lib_selection.h"
/***************************************************************************/

/* X modifiers */
#define XK_ANY_MOD    UINT_MAX
#define XK_NO_MOD     0
#define XK_SWITCH_MOD (1<<13)
/***************************************************************************/
// im add this for "x.c"
extern int borderpx;
extern uint forcemousemod;
extern uint selmasks[SEL_MAX];
#define MSHORTCUTS_SIZE (5)
extern MouseShortcut mshortcuts[MSHORTCUTS_SIZE];
extern unsigned int tripleclicktimeout;
extern unsigned int doubleclicktimeout;

#define COLORNAME_SIZE (256+2)
extern const char *colorname[COLORNAME_SIZE];
extern char ascii_printable[];
extern float cwscale;
extern float chscale;
extern char *font;
extern unsigned int mouseshape;
extern unsigned int mousefg;
extern unsigned int mousebg;
extern unsigned int defaultattr;

extern unsigned int defaultfg;
extern unsigned int defaultbg;
extern unsigned int defaultcs;
extern unsigned int defaultrcs;

extern unsigned int cursorthickness;
extern int bellvolume;
extern uint ignoremod;
#define MAPPEDKEYS_SIZE (1)
extern KeySym mappedkeys[MAPPEDKEYS_SIZE];
#define KEY_SIZE (209)
extern Key key[KEY_SIZE];

#define SHORTCUTS_SIZE (12)
extern Shortcut shortcuts[SHORTCUTS_SIZE];

extern char *shell;
extern double minlatency;
extern double maxlatency;
extern unsigned int blinktimeout;
extern unsigned int cursorshape;
extern unsigned int cols;
extern unsigned int rows;


// from "st.h"
extern char *utmp;
extern char *scroll;
extern char *stty_args;
extern char *vtiden;
extern wchar_t *worddelimiters;
extern int allowaltscreen;
extern int allowwindowops;
extern char *termname;
extern unsigned int tabspaces;
extern unsigned int defaultfg;
extern unsigned int defaultbg;

// from lib_screen.c
extern char *opt_class;
extern char **opt_cmd ;
extern char *opt_embed;
extern char *opt_font ;
extern char *opt_io   ;
extern char *opt_line ;
extern char *opt_name ;
extern char *opt_title;
