/* Arbitrary sizes */
#define ESC_BUF_SIZ   (128*UTF_SIZ)
#define ESC_ARG_SIZ   16
#define STR_BUF_SIZ   ESC_BUF_SIZ
#define STR_ARG_SIZ   ESC_ARG_SIZ
// Globals
extern int cmdfd;
// int iofd = 1; // to lib_printer.c
int tattrset(int attr);
void tsetdirtattr(int attr);
int twrite(const char *buf, int buflen, int show_ctrl);
void ttywrite(const char *s, size_t n, int may_echo);
