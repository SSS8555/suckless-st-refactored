#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
#include <termios.h>
#include <unistd.h>

#include "lib_stdata.h"
#include "lib_st.h"
#include "lib_config.h"
#include "lib_screen.h"
#include "lib_tty.h"
#include "lib_printer.h"
#include "lib_xtools.h"
/**h************************************************************************/
void ttysend(const Arg *arg)
{
	ttywrite(arg->s, strlen(arg->s), 1);
}
/**h************************************************************************/
void sendbreak(const Arg *arg)
{
	if (tcsendbreak(cmdfd, 0))
		perror("Error sending break");
}
/**h************************************************************************/
void toggleprinter(const Arg *arg)
{
	term.mode ^= MODE_PRINT;
}
/**h************************************************************************/
void printscreen(const Arg *arg)
{
	tdump();
}
/**h************************************************************************/
void printsel(const Arg *arg)
{
	tdumpsel();
}
/**h************************************************************************/
void zoom(const Arg *arg)
{
	Arg larg;

	larg.f = usedfontsize + arg->f;
	zoomabs(&larg);
}
/**h************************************************************************/
void zoomreset(const Arg *arg)
{
	Arg larg;

	if (defaultfontsize > 0) {
		larg.f = defaultfontsize;
		zoomabs(&larg);
	}
}
/**h************************************************************************/
void clipcopy(const Arg *dummy)
{
	Atom clipboard;

	free(xsel.clipboard);
	xsel.clipboard = NULL;

	if (xsel.primary != NULL) {
		xsel.clipboard = xstrdup(xsel.primary);
		clipboard = XInternAtom(xw.dpy, "CLIPBOARD", 0);
		XSetSelectionOwner(xw.dpy, clipboard, xw.win, CurrentTime);
	}
}
/**h************************************************************************/
void clippaste(const Arg *dummy)
{
	Atom clipboard;

	clipboard = XInternAtom(xw.dpy, "CLIPBOARD", 0);
	XConvertSelection(xw.dpy, clipboard, xsel.xtarget, clipboard,
			xw.win, CurrentTime);
}
/**h************************************************************************/
void selpaste(const Arg *dummy)
{
	XConvertSelection(xw.dpy, XA_PRIMARY, xsel.xtarget, XA_PRIMARY,
			xw.win, CurrentTime);
}
/**h************************************************************************/
void numlock(const Arg *dummy)
{
	win.mode ^= MODE_NUMLOCK;
}
/***************************************************************************/

