#bash

# st version
VERSION=0.8.4

# Customize below to fit your system

# paths
PREFIX=/usr/local
MANPREFIX=${PREFIX}/share/man

X11INC=/usr/X11R6/include
X11LIB=/usr/X11R6/lib

PKG_CONFIG=pkg-config

# includes and libs
INCS="-I${X11INC} \
       `${PKG_CONFIG} --cflags fontconfig` \
       `${PKG_CONFIG} --cflags freetype2`"
LIBS="-L${X11LIB} -lm -lrt -lX11 -lutil -lXft \
       `${PKG_CONFIG} --libs fontconfig` \
       `${PKG_CONFIG} --libs freetype2`"

# flags
STCPPFLAGS="-DVERSION=\"${VERSION}\" -D_XOPEN_SOURCE=600"
STCFLAGS="${INCS} ${STCPPFLAGS} ${CPPFLAGS} ${CFLAGS}"
STLDFLAGS="${LIBS} ${LDFLAGS}"

# OpenBSD:
#CPPFLAGS = -DVERSION=\"$(VERSION)\" -D_XOPEN_SOURCE=600 -D_BSD_SOURCE
#LIBS = -L$(X11LIB) -lm -lX11 -lutil -lXft \
#       `$(PKG_CONFIG) --libs fontconfig` \
#       `$(PKG_CONFIG) --libs freetype2`

# compiler and linker
# CC = c99
#---------------------------------------------------------------
export DISPLAY="localhost:0"
CC=tcc

echo st build options:
echo "CFLAGS  = ${STCFLAGS}"
echo "LDFLAGS = ${STLDFLAGS}"
echo "CC      = ${CC}"
#---------------------------------------------------------------
#SRC = st.c x.c
rm *.o
rm st

${CC} ${STCFLAGS} -c lib_config.c
${CC} ${STCFLAGS} -c lib_stdata.c
${CC} ${STCFLAGS} -c lib_st.c
${CC} ${STCFLAGS} -c lib_screen.c
${CC} ${STCFLAGS} -c lib_eventloop.c
${CC} ${STCFLAGS} -c lib_shortcuts.c
${CC} ${STCFLAGS} -c lib_selection.c
${CC} ${STCFLAGS} -c lib_tty.c
${CC} ${STCFLAGS} -c lib_printer.c
${CC} ${STCFLAGS} -c lib_xtools.c
${CC} ${STCFLAGS} -c lib_utf8.c
${CC} ${STCFLAGS} -c lib_base64.c
${CC} ${STCFLAGS} -c main.c
${CC} -o st lib_base64.o lib_utf8.o lib_xtools.o lib_printer.o lib_tty.o lib_selection.o lib_shortcuts.o lib_eventloop.o lib_stdata.o lib_st.o lib_screen.o lib_config.o main.o ${STLDFLAGS}

rm *.o

./st mc
