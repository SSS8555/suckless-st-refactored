
//#include "lib_stdata.h"
//#include "lib_st.h"

#include "lib_config.h"
#include "config.h"

// from lib_screen.c
/*static*/ char *opt_class = NULL;
/*static*/ char **opt_cmd  = NULL;
/*static*/ char *opt_embed = NULL;
/*static*/ char *opt_font  = NULL;
/*static*/ char *opt_io    = NULL;
/*static*/ char *opt_line  = NULL;
/*static*/ char *opt_name  = NULL;
/*static*/ char *opt_title = NULL;
